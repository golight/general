#!/bin/bash
docker network create dalaran
cp .env.prod .env
docker-compose down
docker-compose up --force-recreate --build -d