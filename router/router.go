package router

import (
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.com/golight/example-service/modules/quotes/handlers"
	"gitlab.com/golight/example-service/modules/quotes/router"
)

func NewRouter(qh *handlers.QuotesHandler) *chi.Mux {
	r := chi.NewRouter()

	r.Mount("/quotes", router.QuotesRouter(qh))

	r.Get("/swagger", swaggerUI)
	r.Get("/static/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))).ServeHTTP(w, r)
	})

	return r
}
