package cache

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/golight/cache"
	"gitlab.com/golight/example-service/modules/quotes/entity"
	"gitlab.com/golight/example-service/modules/quotes/repository"
)

const (
	quotesCountKey = "count_quotes"
	quotesListKey  = "quotes_list"
	quotesItemKey  = "quotes:"
)

const (
	cacheTTL = 60 * time.Minute
)

// QuotesCache имплементация репозитория цитат с кэшированием
type QuotesCache struct {
	storage repository.Quoteser
	cache   cache.Cacher
}

// NewQuotesCache конструктор репозитория цитат с кэшированием
func NewQuotesCache(storage repository.Quoteser, cache cache.Cacher) repository.Quoteser {
	return &QuotesCache{storage: storage, cache: cache}
}

// Ping проверка доступности репозитория, с помощью проверки доступности базы данных
func (q *QuotesCache) Ping(ctx context.Context) error {
	return q.storage.Ping(ctx)
}

// Count метод получения количества цитат
func (q *QuotesCache) Count(ctx context.Context) (uint64, error) {
	var n uint64
	err := q.cache.Get(ctx, quotesCountKey, &n)
	if err == nil {
		return n, nil
	}

	n, err = q.storage.Count(ctx)
	if err != nil {
		return 0, err
	}

	err = q.cache.Set(ctx, quotesCountKey, n, cacheTTL)
	if err != nil {
		return 0, err
	}

	return n, nil
}

// Create метод создания цитаты
func (q *QuotesCache) Create(ctx context.Context, dto entity.Quote) (int64,error) {
	id, err := q.storage.Create(ctx, dto)
	if err != nil {
		return id, err
	}

	return id, q.cache.Delete(ctx, false, quotesCountKey, quotesListKey)
}

// GetByID метод получения цитаты по идентификатору
func (q *QuotesCache) GetByID(ctx context.Context, quotesID int) (entity.Quote, error) {
	var quote entity.Quote
	key := fmt.Sprintf(quotesItemKey+"%v", quotesID)
	err := q.cache.Get(ctx, key, &quote)
	if err == nil {
		return quote, nil
	}

	quote, err = q.storage.GetByID(ctx, quotesID)
	if err != nil {
		return entity.Quote{}, err
	}

	err = q.cache.Set(ctx, key, quote, cacheTTL)
	if err != nil {
		return entity.Quote{}, err
	}

	return quote, nil
}

// GetList метод получения списка цитат
func (q *QuotesCache) GetList(ctx context.Context) ([]*entity.Quote, error) {
	var quotes []*entity.Quote
	err := q.cache.Get(ctx, quotesListKey, &quotes)
	if err == nil {
		return quotes, nil
	}

	quotes, err = q.storage.GetList(ctx)
	if err != nil {
		return nil, err
	}

	err = q.cache.Set(ctx, quotesListKey, quotes, cacheTTL)
	if err != nil {
		return nil, err
	}

	return quotes, nil
}

// Upsert метод обновления цитаты по идентификатору, в случае отсутствия цитаты, создает новую
func (q *QuotesCache) Upsert(ctx context.Context, entities []*entity.Quote, opts ...interface{}) error {
	err := q.storage.Upsert(ctx, entities)
	if err != nil {
		return err
	}

	err = q.cache.Delete(ctx, true, quotesItemKey+"*")
	if err != nil {
		return err
	}

	return q.cache.Delete(ctx, false, quotesCountKey, quotesListKey)
}

// InitData метод инициализации данных при старте приложения
func (q *QuotesCache) InitData(ctx context.Context, entities []*entity.Quote) (bool, error) {
	var mutated bool
	var err error
	mutated, err = q.storage.InitData(ctx, entities)
	if err != nil {
		return mutated, err
	}

	err = q.cache.Delete(ctx, true, quotesItemKey+"*")
	if err != nil {
		return mutated, err
	}

	return mutated, q.cache.Delete(ctx, false, quotesCountKey, quotesListKey)
}
