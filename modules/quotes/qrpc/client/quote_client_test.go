package client

import (
	"context"
	"errors"
	"log"
	"net"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/golight/example-service/modules/quotes/entity"
	"gitlab.com/golight/example-service/modules/quotes/qrpc/pb"
	qmock "gitlab.com/golight/example-service/modules/quotes/qrpc/client/mocks"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
)

func TestQuotesClient_GetRandomQuote(t *testing.T) {
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name        string
		args        args
		prepareMock func(mock *qmock.MockQuotesActionsRPCClient)
		want        entity.QuoteDTO
		wantErr     bool
	}{
		{
			name: "StatusCode",
			args: args{ctx: context.Background()},
			prepareMock: func(mock *qmock.MockQuotesActionsRPCClient) {
				mock.EXPECT().GetRandom(gomock.Any(), &emptypb.Empty{}).
					Times(1).
					Return(&pb.Quote{}, nil)
			},
			want:    entity.QuoteDTO{},
			wantErr: false,
		},
		{
			name: "Error q.rpc.GetRandom()",
			args: args{ctx: context.Background()},
			prepareMock: func(mock *qmock.MockQuotesActionsRPCClient) {
				mock.EXPECT().GetRandom(gomock.Any(), &emptypb.Empty{}).
					Times(1).
					Return(&pb.Quote{}, errors.New("some error"))
			},
			want:    entity.QuoteDTO{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rpcClient := qmock.NewMockQuotesActionsRPCClient(gomock.NewController(t))

			tt.prepareMock(rpcClient)

			quotes := &QuotesClient{
				rpc: rpcClient,
			}

			quote, err := quotes.GetRandomQuote(context.Background())
			if (err != nil) != tt.wantErr {
				t.Errorf("GetRandomQuote() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, quote)
		})
	}
}

func TestQuotesClient_HealthCheck(t *testing.T) {
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name        string
		args        args
		prepareMock func(mock *qmock.MockQuotesActionsRPCClient)
		want        entity.QuoteDTO
		wantErr     bool
	}{
		{
			name: "StatusCode",
			args: args{ctx: context.Background()},
			prepareMock: func(mock *qmock.MockQuotesActionsRPCClient) {
				mock.EXPECT().HealthCheck(gomock.Any(), &emptypb.Empty{}).
					Times(1).
					Return(&emptypb.Empty{}, nil)
			},
			want:    entity.QuoteDTO{},
			wantErr: false,
		},
		{
			name: "Error q.rpc.HealthCheck()",
			args: args{ctx: context.Background()},
			prepareMock: func(mock *qmock.MockQuotesActionsRPCClient) {
				mock.EXPECT().HealthCheck(gomock.Any(), &emptypb.Empty{}).
					Times(1).
					Return(&emptypb.Empty{}, errors.New("some error"))
			},
			want:    entity.QuoteDTO{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rpcClient := qmock.NewMockQuotesActionsRPCClient(gomock.NewController(t))

			tt.prepareMock(rpcClient)

			quotes := &QuotesClient{
				rpc: rpcClient,
			}

			err := quotes.HealthCheck(context.Background())
			if (err != nil) != tt.wantErr {
				t.Errorf("GetRandomQuote() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestNewQuotesClient(t *testing.T) {
	type args struct {
		addr string
	}
	tests := []struct {
		name string
		args args
		want *QuotesClient
	}{
		{
			name: "StatusCode",
			args: args{
				addr: ":0",
			},
			want: &QuotesClient{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			lis, err := net.Listen("tcp", tt.args.addr) // fake server
			if err != nil {
				log.Fatalf("failed to listen: %v", err)
			}

			s := grpc.NewServer()

			go func() {
				if err := s.Serve(lis); err != nil {
					log.Fatalf("failed to serve: %v", err)
				}
			}()

			defer s.Stop()

			quotesClient := NewQuotesClient(tt.args.addr)

			assert.IsType(t, tt.want, quotesClient)
		})
	}
}
