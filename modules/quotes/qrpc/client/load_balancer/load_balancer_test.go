package load_balancer

import (
	"context"
	"errors"
	"sync"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"gitlab.com/golight/example-service/modules/quotes/qrpc/client"
	qmock "gitlab.com/golight/example-service/modules/quotes/qrpc/client/mocks"
)

type GeneratorQuoteserMock struct {
	rpcQuoteserMocks []*qmock.MockRPCQuoteser
	t                *testing.T
}

func NewGeneratorQuoteserMock(t *testing.T, n int) *GeneratorQuoteserMock {
	return &GeneratorQuoteserMock{
		rpcQuoteserMocks: make([]*qmock.MockRPCQuoteser, n),
		t:                t,
	}
}

// [from, to)
func (g *GeneratorQuoteserMock) PrepareMocks(from, to int, prepare func(rpcQuoteserMock *qmock.MockRPCQuoteser)) *GeneratorQuoteserMock {
	for i := from; i < to; i++ {
		mock := g.rpcQuoteserMocks[i]
		if mock == nil {
			mock = qmock.NewMockRPCQuoteser(gomock.NewController(g.t))
			g.rpcQuoteserMocks[i] = mock
		}
		prepare(mock)
	}
	return g
}

func (g *GeneratorQuoteserMock) GenerateClients() []*ClientStatus {
	result := make([]*ClientStatus, len(g.rpcQuoteserMocks))
	for i := range g.rpcQuoteserMocks {
		result[i] = &ClientStatus{
			client:  g.rpcQuoteserMocks[i],
			backoff: 10 * time.Minute,
		}
	}
	return result
}

func TestQuotesLoadBalancer_Eliminate(t *testing.T) {
	tests := []struct {
		name             string
		prepareGenerator func() *GeneratorQuoteserMock
		wantActive       int
		wantInactive     int
	}{
		{
			name: "Eliminate: when last 5 clients inactivated",
			prepareGenerator: func() *GeneratorQuoteserMock {
				return NewGeneratorQuoteserMock(t, 15).
					PrepareMocks(0, 10, func(rpcQuoteserMock *qmock.MockRPCQuoteser) {
						rpcQuoteserMock.EXPECT().HealthCheck(gomock.Any()).Times(1).Return(nil)
					}).
					PrepareMocks(10, 15, func(rpcQuoteserMock *qmock.MockRPCQuoteser) {
						rpcQuoteserMock.EXPECT().HealthCheck(gomock.Any()).Times(1).Return(errors.New("test"))
					})
			},
			wantActive:   10,
			wantInactive: 5,
		},
		{
			name: "Eliminate: when all clients inactivated",
			prepareGenerator: func() *GeneratorQuoteserMock {
				return NewGeneratorQuoteserMock(t, 15).
					PrepareMocks(0, 15, func(rpcQuoteserMock *qmock.MockRPCQuoteser) {
						rpcQuoteserMock.EXPECT().HealthCheck(gomock.Any()).Times(1).Return(errors.New("test"))
					})
			},
			wantActive:   0,
			wantInactive: 15,
		},
		{
			name: "Eliminate: when all clients activated",
			prepareGenerator: func() *GeneratorQuoteserMock {
				return NewGeneratorQuoteserMock(t, 15).
					PrepareMocks(0, 15, func(rpcQuoteserMock *qmock.MockRPCQuoteser) {
						rpcQuoteserMock.EXPECT().HealthCheck(gomock.Any()).Times(1).Return(nil)
					})
			},
			wantActive:   15,
			wantInactive: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			generator := tt.prepareGenerator()
			r := &QuotesLoadBalancer{
				inactiveClients:    nil,
				activeClients:      generator.GenerateClients(),
				processTick:        1 * time.Second,
				healthCheckTimeout: 500 * time.Millisecond,
				nextIndex:          0,
				initialBackoff:     10 * time.Minute,
				maxBackoff:         6 * time.Hour,
				mux:                sync.Mutex{},
			}
			r.Eliminate()

			activeClients := r.activeClients
			inactiveClients := r.inactiveClients
			if len(activeClients) != tt.wantActive {
				t.Errorf("Eliminate() activeClients got = %v, want %v", len(activeClients), tt.wantActive)
			}
			if len(inactiveClients) != tt.wantInactive {
				t.Errorf("Eliminate() inactiveClients got = %v, want %v", len(inactiveClients), tt.wantInactive)
			}
		})
	}
}

func TestQuotesLoadBalancer_Recover(t *testing.T) {
	tests := []struct {
		name             string
		prepareGenerator func() *GeneratorQuoteserMock
		wantActive       int
		wantInactive     int
	}{
		{
			name: "Recover: when last 5 clients activated",
			prepareGenerator: func() *GeneratorQuoteserMock {
				return NewGeneratorQuoteserMock(t, 15).
					PrepareMocks(0, 10, func(rpcQuoteserMock *qmock.MockRPCQuoteser) {
						rpcQuoteserMock.EXPECT().HealthCheck(gomock.Any()).Times(1).Return(errors.New("test"))
					}).
					PrepareMocks(10, 15, func(rpcQuoteserMock *qmock.MockRPCQuoteser) {
						rpcQuoteserMock.EXPECT().HealthCheck(gomock.Any()).Times(1).Return(nil)
					})
			},
			wantActive:   5,
			wantInactive: 10,
		},
		{
			name: "Recover: when all clients activated",
			prepareGenerator: func() *GeneratorQuoteserMock {
				return NewGeneratorQuoteserMock(t, 15).
					PrepareMocks(0, 15, func(rpcQuoteserMock *qmock.MockRPCQuoteser) {
						rpcQuoteserMock.EXPECT().HealthCheck(gomock.Any()).Times(1).Return(nil)
					})
			},
			wantActive:   15,
			wantInactive: 0,
		},
		{
			name: "Recover: when no clients were activated",
			prepareGenerator: func() *GeneratorQuoteserMock {
				return NewGeneratorQuoteserMock(t, 15).
					PrepareMocks(0, 15, func(rpcQuoteserMock *qmock.MockRPCQuoteser) {
						rpcQuoteserMock.EXPECT().HealthCheck(gomock.Any()).Times(1).Return(errors.New("test"))
					})
			},
			wantActive:   0,
			wantInactive: 15,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			generator := tt.prepareGenerator()
			r := &QuotesLoadBalancer{
				inactiveClients:    generator.GenerateClients(),
				activeClients:      nil,
				processTick:        1 * time.Second,
				healthCheckTimeout: 500 * time.Millisecond,
				nextIndex:          0,
				initialBackoff:     10 * time.Minute,
				maxBackoff:         500 * time.Millisecond,
				mux:                sync.Mutex{},
			}
			r.Recover()

			activeClients := r.activeClients
			inactiveClients := r.inactiveClients
			if len(activeClients) != tt.wantActive {
				t.Errorf("Recover() activeClients got = %v, want %v", len(activeClients), tt.wantActive)
			}
			if len(inactiveClients) != tt.wantInactive {
				t.Errorf("Recover() inactiveClients got = %v, want %v", len(inactiveClients), tt.wantInactive)
			}
		})
	}
}

func TestQuotesLoadBalancer_Next(t *testing.T) {
	tests := []struct {
		name          string
		activeClients []*ClientStatus
		countNextCall int
	}{
		{
			name:          "Next over activeCount",
			activeClients: make([]*ClientStatus, 3),
			countNextCall: 7,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &QuotesLoadBalancer{
				inactiveClients: nil,
				activeClients:   tt.activeClients,
				nextIndex:       0,
				mux:             sync.Mutex{},
			}

			ln := len(tt.activeClients)
			for i := 0; i < ln; i++ {
				tt.activeClients[i] = &ClientStatus{
					client: qmock.NewMockRPCQuoteser(gomock.NewController(t)),
				}
			}

			for i, j := 0, 0; i < tt.countNextCall; i, j = i+1, j+1 {
				if j == ln {
					j = 0
				}
				if got := r.Next(); got != tt.activeClients[j].client {
					t.Errorf("Next() goted client is not same")
				}
			}
		})
	}
}

func TestQuotesLoadBalancer_NextWhenLenActiveCountIs0(t *testing.T) {
	activeClients := make([]*ClientStatus, 0)
	r := &QuotesLoadBalancer{
		inactiveClients: nil,
		activeClients:   activeClients,
		nextIndex:       0,
		mux:             sync.Mutex{},
	}

	if got := r.Next(); got != nil {
		t.Errorf("Next() got %s, want nil", got)
		return
	}
}

func TestNewLoadBalancer(t *testing.T) {
	tests := []struct {
		name             string
		countClientsPool int
		want             *QuotesLoadBalancer
	}{
		{
			name:             "NewLoadBalancer",
			countClientsPool: 3,
			want: &QuotesLoadBalancer{
				processTick:        888,
				healthCheckTimeout: 999,
				nextIndex:          0,
				initialBackoff:     10 * time.Minute,
				maxBackoff:         6 * time.Hour,
				mux:                sync.Mutex{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			clientsPool := make([]client.RPCQuoteser, tt.countClientsPool)
			for i := 0; i < tt.countClientsPool; i++ {
				clientsPool[i] = qmock.NewMockRPCQuoteser(gomock.NewController(t))
			}

			if got := NewLoadBalancer(clientsPool,
				WithHealthCheckTimeout(tt.want.healthCheckTimeout),
				WithProcessTick(tt.want.processTick)); got.processTick != tt.want.processTick ||
				got.healthCheckTimeout != tt.want.healthCheckTimeout ||
				got.nextIndex != tt.want.nextIndex ||
				got.initialBackoff != tt.want.initialBackoff ||
				got.maxBackoff != tt.want.maxBackoff ||
				len(got.activeClients) != len(clientsPool) {
				t.Errorf("NewLoadBalancer() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestQuotesLoadBalancer_ProcessClients(t *testing.T) {
	tests := []struct {
		name         string
		processTick  time.Duration
		durationWork time.Duration
	}{
		{
			name:         "ProcessClients tick 10ms for 1s",
			processTick:  10 * time.Millisecond,
			durationWork: 1 * time.Second,
		},
		{
			name:         "ProcessClients tick 100ms for 1s",
			processTick:  100 * time.Millisecond,
			durationWork: 1 * time.Second,
		},
		{
			name:         "ProcessClients tick 500ms for 2s",
			processTick:  500 * time.Millisecond,
			durationWork: 2 * time.Second,
		},
		{
			name:         "ProcessClients tick 200ms for 3s",
			processTick:  200 * time.Millisecond,
			durationWork: 3 * time.Second,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.durationWork += 5 * time.Millisecond // so as not to get caught time borderline value

			count := int(tt.durationWork / tt.processTick)
			activeClient := qmock.NewMockRPCQuoteser(gomock.NewController(t))
			inactiveClient := qmock.NewMockRPCQuoteser(gomock.NewController(t))

			mxActive := sync.Mutex{}
			mxInactive := sync.Mutex{}
			countActiveClientCall := 0
			countInactiveClientCall := 0
			activeClient.EXPECT().HealthCheck(gomock.Any()).DoAndReturn(func(context.Context) error {
				mxActive.Lock()
				countActiveClientCall++
				mxActive.Unlock()
				return nil
			}).AnyTimes()
			inactiveClient.EXPECT().HealthCheck(gomock.Any()).DoAndReturn(func(context.Context) error {
				mxInactive.Lock()
				countInactiveClientCall++
				mxInactive.Unlock()
				return errors.New("test")
			}).AnyTimes()

			q := &QuotesLoadBalancer{
				activeClients:   []*ClientStatus{{client: activeClient}},
				inactiveClients: []*ClientStatus{{client: inactiveClient}},
				processTick:     tt.processTick,
			}

			go q.ProcessClients()
			time.Sleep(tt.durationWork)

			mxActive.Lock()
			mxInactive.Lock()
			defer mxActive.Unlock()
			defer mxInactive.Unlock()
			if countActiveClientCall != count+1 {
				t.Errorf("ProcessClients got active count call %d, want %d", countActiveClientCall, count+1)
			}
			if countInactiveClientCall != count {
				t.Errorf("ProcessClients got inactive count call %d, want %d", countInactiveClientCall, count)
			}
		})
	}
}
