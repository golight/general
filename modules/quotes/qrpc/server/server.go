package server

import (
	"context"

	"gitlab.com/golight/example-service/modules/quotes/qrpc/pb"
	"gitlab.com/golight/example-service/modules/quotes/service"
	"google.golang.org/protobuf/types/known/emptypb"
)

// QuotesRPCServer имплементация rpc сервера цитат
type QuotesRPCServer struct {
	pb.UnimplementedQuotesActionsRPCServer
	srv service.Quoteser
}

// NewQuotesRPCServer конструктор rpc сервера цитат
func NewQuotesRPCServer(srv service.Quoteser) pb.QuotesActionsRPCServer {
	return &QuotesRPCServer{srv: srv}
}

// GetRandom rpc запрос на получение случайной цитаты
func (q *QuotesRPCServer) GetRandom(ctx context.Context, _ *emptypb.Empty) (*pb.Quote, error) {
	quote, err := q.srv.GetRandomQuote(ctx)
	if err != nil {
		return nil, err
	}

	return &pb.Quote{
		Id:     int64(quote.ID),
		Text:   quote.Text,
		Author: quote.Author,
	}, nil
}

// HealthCheck rpc запрос на проверку доступности сервера
func (q *QuotesRPCServer) HealthCheck(ctx context.Context, _ *emptypb.Empty) (*emptypb.Empty, error) {
	err := q.srv.Check(ctx)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}
