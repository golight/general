package server

import (
	"context"
	"errors"
	"net/http"
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"
	"gitlab.com/golight/example-service/modules/quotes/entity"
	"gitlab.com/golight/example-service/modules/quotes/qrpc/pb"
	qmock "gitlab.com/golight/example-service/modules/quotes/handlers/mocks"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

func TestQuotesRPCServer_GetRandom(t *testing.T) {
	var quote = entity.QuoteDTO{
		ID:     123,
		Text:   "test_text",
		Author: "test_author",
	}

	tests := []struct {
		name       string
		prepare    func(quoteser *qmock.MockQuoteser)
		wantStatus int
		wantErr    bool
	}{
		{
			name: "GetRandom success",
			prepare: func(quoteserMock *qmock.MockQuoteser) {
				quoteserMock.EXPECT().GetRandomQuote(gomock.Any()).
					Times(1).
					Return(quote, nil)
			},
			wantStatus: http.StatusOK,
		},
		{
			name: "GetRandom error",
			prepare: func(quoteserMock *qmock.MockQuoteser) {
				quoteserMock.EXPECT().GetRandomQuote(gomock.Any()).
					Times(1).
					Return(quote, errors.New("test"))
			},
			wantStatus: http.StatusInternalServerError,
			wantErr:    true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteserMock := qmock.NewMockQuoteser(gomock.NewController(t))
			tt.prepare(quoteserMock)
			quotesRPCServer := NewQuotesRPCServer(quoteserMock)

			// Создание запроса gRPC
			req := &emptypb.Empty{}
			ctx := context.Background()
			result, err := quotesRPCServer.GetRandom(ctx, req)

			if tt.wantErr {
				// Проверка, что ожидается ошибка
				if err == nil {
					t.Error("TestQuotesRPCServer expected error, but got nil")
				}
				return
			}

			// Проверка статуса ответа
			if status.Code(err) != codes.OK {
				t.Errorf("TestQuotesRPCServer status got: %s, want %s", status.Code(err), codes.OK)
			}

			// Проверка структуры ответа
			expectedQuote := &pb.Quote{
				Id:     int64(quote.ID),
				Text:   quote.Text,
				Author: quote.Author,
			}
			if !reflect.DeepEqual(result, expectedQuote) {
				t.Error("TestQuotesRPCServer response does not match expected result")
			}
		})
	}
}

func TestQuotesRPCServer_HealthCheck(t *testing.T) {
	tests := []struct {
		name       string
		prepare    func(quoteser *qmock.MockQuoteser)
		wantStatus int
		wantErr    bool
	}{
		{
			name: "HealthCheck success",
			prepare: func(quoteserMock *qmock.MockQuoteser) {
				quoteserMock.EXPECT().Check(gomock.Any()).
					Times(1).
					Return(nil)
			},
			wantStatus: http.StatusOK,
		},
		{
			name: "HealthCheck error",
			prepare: func(quoteserMock *qmock.MockQuoteser) {
				quoteserMock.EXPECT().Check(gomock.Any()).
					Times(1).
					Return(errors.New("test"))
			},
			wantStatus: http.StatusInternalServerError,
			wantErr:    true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			quoteserMock := qmock.NewMockQuoteser(gomock.NewController(t))
			tt.prepare(quoteserMock)
			quotesRPCServer := NewQuotesRPCServer(quoteserMock)

			// Создание запроса gRPC
			req := &emptypb.Empty{}
			ctx := context.Background()
			result, err := quotesRPCServer.HealthCheck(ctx, req)

			if tt.wantErr {
				// Проверка, что ожидается ошибка
				if err == nil {
					t.Error("TestQuotesRPCServer_HealthCheck expected error, but got nil")
				}
				return
			}

			// Проверка статуса ответа
			if status.Code(err) != codes.OK {
				t.Errorf("TestQuotesRPCServer_HealthCheck status got: %s, want %s", status.Code(err), codes.OK)
			}

			// Проверка структуры ответа
			expectedResult := &emptypb.Empty{}
			if !reflect.DeepEqual(result, expectedResult) {
				t.Error("TestQuotesRPCServer_HealthCheck response does not match expected result")
			}
		})
	}
}

