package handlers

import (
	"context"
	"encoding/json"
	"net/http"

	"gitlab.com/golight/example-service/modules/quotes/service"

)

type QuotesHandler struct {
	quotesService service.Quoteser
}

func NewQuotesHandler(quotesService service.Quoteser) *QuotesHandler {
	return &QuotesHandler{
		quotesService: quotesService,
	}
}

func (qh *QuotesHandler) GetRandomQuoteHandler(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()

	quote, err := qh.quotesService.GetRandomQuote(ctx)
	if err != nil {
		http.Error(w, "Failed to get random quote", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	err = json.NewEncoder(w).Encode(quote)
	if err != nil {
		http.Error(w, "Failed to encode JSON response", http.StatusInternalServerError)
		return
	}
}

func (qh *QuotesHandler) CheckHandler(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()

	err := qh.quotesService.Check(ctx)
	if err != nil {
		http.Error(w, "Service check failed", http.StatusInternalServerError)
		return
	}

	_, err = w.Write([]byte("Service is available"))
	if err != nil {
		http.Error(w, "Failed to write response", http.StatusInternalServerError)
		return
	}
}
