package main

import (
	"context"
	"log"
	"time"

	"gitlab.com/golight/example-service/modules/quotes/qrpc/client"
)

func main() {

	cli := client.NewQuotesClient("localhost:4080")

	for {
		res, err := cli.GetRandomQuote(context.Background())

		if err != nil {
			log.Fatalf("Ошибка при вызове RPC: %v", err)
		}

		log.Printf("\nЦитата: %s\nАвтор цитаты: %s\n\n", res.Text, res.Author)
		

		time.Sleep(time.Second*2)
	}

}
